package com.example.shop_bot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShopBotApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShopBotApplication.class, args);
    }

}
