package com.example.shop_bot.enums;

public enum RoleName {
    USER, ADMIN, SUPPER_ADMIN
}
