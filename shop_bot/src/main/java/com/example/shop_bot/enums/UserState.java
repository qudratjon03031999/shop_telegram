package com.example.shop_bot.enums;

public enum UserState {
    CHOOSE_LANGUAGE,

    EDIT,
    DEFAULT,

    ///////

    CLIENT_SEND_LOCATION,
    CLIENT_SEND_PHONE_NUMBER
}
