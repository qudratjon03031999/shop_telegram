package com.example.shop_bot.enums;

public enum Gender {
    MALE, FEMALE
}
