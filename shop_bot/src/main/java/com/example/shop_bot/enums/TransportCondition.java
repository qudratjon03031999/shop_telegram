package com.example.shop_bot.enums;

public enum TransportCondition {
    GREAT("", "GREAT", "ОТЛИЧНОЕ", "IDEAL", "👍"),
    GOOD("", "GOOD", "ХОРОШЕЕ", "YAXSHI", "👍"),
    MEDIUM("", "MEDIUM", "СРЕДНЕЕ", "O'RTACHA", "👍"),
    BAD("", "BAD", "ТРЕБУЕТ РЕМОНТА", "TA'MIR TALAB", "👍");


    private final String titleRu;
    private final String titleUz;
    private final String titleEn;
    private final String description;
    private final String icon;

    TransportCondition(String description, String titleEn, String titleRu, String titleUz, String icon) {
        this.titleRu = titleRu;
        this.titleUz = titleUz;
        this.titleEn = titleEn;
        this.description = description;
        this.icon = icon;
    }

    public String getIcon() {
        return icon;
    }

    public String getTitleRu() {
        return titleRu;
    }

    public String getTitleUz() {
        return titleUz;
    }

    public String getTitleEn() {
        return titleEn;
    }


    public String getDescription() {
        return description;
    }

}
