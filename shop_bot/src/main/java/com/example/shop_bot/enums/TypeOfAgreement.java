package com.example.shop_bot.enums;

public enum TypeOfAgreement {
    FREE, PRICE, EXCHANGE
}
