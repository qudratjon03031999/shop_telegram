package com.example.shop_bot.enums;

public enum QualityType {
    ORIGINAL, AVERAGE, LOW
}
