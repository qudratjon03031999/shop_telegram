package com.example.shop_bot.sercurity;

import com.example.shop_bot.config.UsersDetails;
import com.example.shop_bot.entity.User;
import com.example.shop_bot.entity.addons.Role;
import com.example.shop_bot.enums.RoleName;
import com.example.shop_bot.exception.ExceptionSend;
import com.example.shop_bot.payload.ApiResponseModel;
import com.example.shop_bot.payload.JwtResponse;
import com.example.shop_bot.payload.ReqUser;
import com.example.shop_bot.payload.ReqUserProfile;
import com.example.shop_bot.repository.RoleRepository;
import com.example.shop_bot.repository.UserRepository;
import com.example.shop_bot.telegramBot.Bot;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @description: TODO
 * @projectName koinot_market
 * @date: 15 February 2022 $
 * @time: 22:59 $
 * @author: Qudratjon Komilov
 */

@Service
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@Slf4j
public class AuthService {
    UserRepository userRepository;
    PasswordEncoder passwordEncoder;
    RoleRepository roleRepository;
    JwtTokenProvider jwtTokenProvider;
    ExceptionSend exceptionSend;
    AuthenticationManager authenticate;
    UsersDetails userDetails;
    Bot bot;


    @Value("${app.expiredTime}")
    private Long expiredTime;


    public UserDetails loadUserById(Long userId) {
        Optional<User> byPhoneNumber = userRepository.findById(userId);
        if (byPhoneNumber.isPresent()) {
            return byPhoneNumber.get();
        } else {
            exceptionSend.senException(" loadUserById => " + userId, null, null);
            log.error(" loadUserById => " + userId);
            return null;
        }
    }

    public HttpEntity<ApiResponseModel> register(ReqUser reqUser) {
        String code = generatePassword();
        HttpServletRequest request = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
        String ip = request.getRemoteAddr();
        if (!(ip.length() > 0)) {
            return ResponseEntity.badRequest().body(new ApiResponseModel(HttpStatus.CONFLICT.value(),
                    "invalid ip address"));
        }
        Optional<User> userOptional = userRepository.findByPhoneNumber(reqUser.getPhoneNumber());
        User user = new User();

        if (userOptional.isPresent()) {
            System.err.println(user.isEnabled());
            if (user.isEnabled()) {
                return ResponseEntity.status(HttpStatus.CONFLICT).body(new ApiResponseModel(HttpStatus.CONFLICT.value(),
                        "you already exist",
                        null));
            } else {
                user = userOptional.get();
            }

        }

        user.setIpAddress(ip);
        user.setExpiredCode(new Date(new Date().getTime() + expiredTime));
        user.setEnabled(false);
        user.setVerifyCode(code);
        user.setPassword(passwordEncoder.encode(reqUser.getPassword()));
        user.setRoles(roleRepository.findAllByName(RoleName.USER));
        user.setPhoneNumber(reqUser.getPhoneNumber());
        user.setLastName(reqUser.getLastName());
        user.setFirstName(reqUser.getFirstName());
        userRepository.save(user);


        sendMessage(user.getPhoneNumber(), "koinot " + code);
        sendTelegramCode("853564241", "koinot " + code);
        sendTelegramCode("747505857", "koinot " + code);

        HashMap<String, String> capitalCities = new HashMap<String, String>();

        // Add keys and values (expiredCode, phoneNumber)
        capitalCities.put("expiredTime", String.valueOf(expiredTime));
        capitalCities.put("phoneNumber", user.getPhoneNumber());


        return ResponseEntity.status(HttpStatus.OK).body(new ApiResponseModel(HttpStatus.OK.value(),
                "we sent you the code to confirm it, you have " + expiredTime / 60000 + " minutes",
                capitalCities));


    }

    public HttpEntity<ApiResponseModel> login(ReqUser reqUser) {
        HttpServletRequest request = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
        String ip = request.getRemoteAddr();
        if (ip.length() > 0) {
            return ResponseEntity.status(HttpStatus.ACCEPTED)
                    .body(new ApiResponseModel(HttpStatus.OK.value(),
                            "login",
                            getApiToken(reqUser.getPhoneNumber(), reqUser.getPassword())));
        }
        return ResponseEntity.badRequest()
                .body(new ApiResponseModel(HttpStatus.BAD_REQUEST.value(), "invalid ip address", null));
    }

    public void sendTelegramCode(String id, String code) {
        try {
//            new RestTemplate().getForEntity(
//                    "https://api.telegram.org/5131540394:AAHQAFVAzT4WelfdljntvigC2uVcXPzlkjI/sendMessage?chat_id=" + id + "&text=" + code,
//                    String.class);
            SendMessage message = new SendMessage();
            message.setChatId(id);
            message.setText(code);

            bot.execute(message);
        } catch (Exception e) {

            exceptionSend.senException(" sendTelegramCode => ", e, null);
            log.error(" sendTelegramCode => ", e.getMessage());

        }

    }

    public void sendMessage(String phoneNumber, String code) {
        try {

            log.info("send sms to  " + phoneNumber);
        } catch (Exception e) {
            exceptionSend.senException(" sendMessage => ", e, null);
            log.error(" sendMessage => ", e);
        }
    }

    public String generatePassword() {
        return RandomStringUtils.random(6, false, true);
    }

    public HttpEntity<ApiResponseModel> verify(String code, String phoneNumber) {
        HttpServletRequest request = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();

        String ip = request.getRemoteAddr();

        Optional<User> byPhoneNumber = userRepository.findByPhoneNumber(phoneNumber);
        if (byPhoneNumber.isPresent()) {
            User user = byPhoneNumber.get();
            if (ip.isEmpty()) {
                return ResponseEntity.status(HttpStatus.CONFLICT)
                        .body(new ApiResponseModel(HttpStatus.CONFLICT.value(), "invalid ip address"));
            }
            if (!ip.equals(user.getIpAddress())) {
                return ResponseEntity.status(HttpStatus.CONFLICT)
                        .body(new ApiResponseModel(HttpStatus.CONFLICT.value(), "this is another device"));
            }
            if (user.getVerifyCode().equals(code)) {
                if (user.getExpiredCode().getTime() < new Date().getTime()) {
                    return ResponseEntity.status(HttpStatus.FORBIDDEN).body(new ApiResponseModel(HttpStatus.FORBIDDEN.value(),
                            "code expired"));
                }
                user.setEnabled(true);

                userRepository.save(user);
                return ResponseEntity.status(HttpStatus.ACCEPTED).body(new ApiResponseModel(HttpStatus.ACCEPTED.value(),
                        "successful",
                        new JwtResponse(jwtTokenProvider.generateTokenForUser(user),
                                user.getPhoneNumber(),
                                user.getLastName(),
                                user.getFirstName(),
                                user.getRoles().stream().map(Role::getName).collect(
                                        Collectors.toList()))));
            }
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body(new ApiResponseModel(HttpStatus.FORBIDDEN.value(),
                    "code invalid"));
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ApiResponseModel(HttpStatus.BAD_REQUEST.value(), "this has not phone number"));
    }

    public HttpEntity<ApiResponseModel> sendCode(String phoneNumber) {
        try {

            HttpServletRequest request = ((ServletRequestAttributes) Objects.requireNonNull(RequestContextHolder.getRequestAttributes())).getRequest();
            String ip = request.getRemoteAddr();
            if (!(ip.length() > 0)) {
                return ResponseEntity.badRequest().body(new ApiResponseModel(HttpStatus.CONFLICT.value(),
                        "invalid ip address"));
            }

            Optional<User> byPhoneNumber = userRepository.findByPhoneNumber(phoneNumber);
            if (byPhoneNumber.isPresent()) {
                User user = byPhoneNumber.get();
                user.setIpAddress(ip);
                String code = generatePassword();
                user.setVerifyCode(code);
                user.setExpiredCode(new Date(new Date().getTime() + expiredTime));
                userRepository.save(user);
                sendTelegramCode(user.getTelegramId().toString(), code);
                sendMessage(user.getPhoneNumber(), code);
                return ResponseEntity.status(HttpStatus.OK).body(new ApiResponseModel(HttpStatus.OK.value(),
                        "send code"));
            }
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ApiResponseModel(HttpStatus.BAD_REQUEST.value(),
                    "this has not phone number"));
        } catch (Exception e) {
            exceptionSend.senException(" controller recode => ", e, null);
            log.error(" controller recode => ", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new ApiResponseModel(HttpStatus.INTERNAL_SERVER_ERROR.value(),
                    e.getMessage()));
        }
    }

    public HttpEntity<ApiResponseModel> newPassword(String password, User user) {
        try {
            user.setPassword(passwordEncoder.encode(password));
            userRepository.save(user);
            return ResponseEntity.status(HttpStatus.ACCEPTED).body(new ApiResponseModel(HttpStatus.ACCEPTED.value(),
                    "successful",
                    null));
        } catch (Exception e) {
            exceptionSend.senException(" newPassword => " + password, null, null);
            log.error(" newPassword => " + password);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ApiResponseModel(HttpStatus.BAD_REQUEST.value(),
                    "error"));
        }
    }

    public HttpEntity<ApiResponseModel> editMe(User user, ReqUserProfile editData) {
        user.setPassword(passwordEncoder.encode(editData.getPassword()));
        user.setFirstName(editData.getFirstName().substring(0, 1).toUpperCase() + editData.getFirstName().substring(1));
        user.setLastName(editData.getLastName().substring(0, 1).toUpperCase() + editData.getLastName().substring(1));
        user.setAddress(editData.getAddress());
        user.setPhoneNumber(editData.getPhoneNumber());
        user.setUsername(editData.getUsername());
        user.setLanguage(editData.getLanguage());
        user.setAddress(editData.getAddress());
        user.setLongitude(editData.getLongitude());
        user.setLatitude(editData.getLatitude());
        userRepository.save(user);
        return ResponseEntity.status(HttpStatus.ACCEPTED).body(new ApiResponseModel(HttpStatus.ACCEPTED.value(),
                "save user",
                user));
    }

    public JwtResponse getApiToken(String phoneNumber, String password) {
        Authentication authentication = authenticate.authenticate(new UsernamePasswordAuthenticationToken(phoneNumber,
                password));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        User userPrincipal = userDetails.foundUser(phoneNumber);
        return new JwtResponse(jwtTokenProvider.generateTokenForUser(userPrincipal),
                userPrincipal.getPhoneNumber(),
                userPrincipal.getLastName(),
                userPrincipal.getFirstName(),
                userPrincipal.getRoles().stream().map(Role::getName).collect(
                        Collectors.toList()));
    }

}
