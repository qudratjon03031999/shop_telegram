package com.example.shop_bot.component;

import com.example.shop_bot.entity.Messages;
import com.example.shop_bot.entity.User;
import com.example.shop_bot.entity.addons.Role;
import com.example.shop_bot.enums.Msg;
import com.example.shop_bot.enums.RoleName;
import com.example.shop_bot.enums.UserState;
import com.example.shop_bot.repository.MessagesRepository;
import com.example.shop_bot.repository.RoleRepository;
import com.example.shop_bot.repository.UserRepository;
import com.example.shop_bot.service.bot.BotAnswerString;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
@RequiredArgsConstructor
public class DataLoader implements CommandLineRunner {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final MessagesRepository messagesRepository;

    private final PasswordEncoder passwordEncoder;


    @Value("${spring.sql.init.mode}")
    private String initialMode;

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


    @Override
    public void run(String... args) {

        if (initialMode.equals("always")) {

            roleRepository.save(new Role(1, RoleName.USER));
            roleRepository.save(new Role(2, RoleName.ADMIN));
            roleRepository.save(new Role(3, RoleName.SUPPER_ADMIN));

            List<Messages> messages = new ArrayList<>();

            for (Msg value : Msg.values()) {

                messages.add(new Messages(value.name(), value.getDescription(),
                        value.getTitleUz() == null ? value.getTitleEn() : value.getTitleUz(),
                        value.getTitleRu() == null ? value.getTitleEn() : value.getTitleRu(),
                        value.getTitleEn()));

            }

            messagesRepository.saveAll(messages);


            List<Role> save = new ArrayList<>();

            if (roleRepository.count() == 0) {

                for (int i = 0; i < RoleName.values().length; i++) {
                    save.add(roleRepository.save(new Role(i, RoleName.values()[i])));
                }


            } else {
                save = new ArrayList<>(roleRepository.findAll());
            }


            User ilyosUser = new User();
            User qudratjon = new User();

            if (userRepository.count() == 0) {
                userRepository.deleteAll();
                for (int i = 0; i < 5; i++) {
                    User user = new User();
                    user.setPhoneNumber("+99891772" + i);
                    user.setLastName("Qudratjon" + i);
                    user.setFirstName("Komilov" + i);
                    user.setState(UserState.DEFAULT);
                    user.setTelegramId((long) (747505858 + i));
                    user.setLanguage(BotAnswerString.uzl);
                    user.setPassword(passwordEncoder.encode("koinot" + i));
                    user.setUsername("username" + i);
                    user.setAddress("Toshkent shahar oybek metro" + i);
                    user.setLatitude(13249847987D + i);
                    user.setLongitude(49846846814D + i);
                    user.setBalance(123000D + i);
                    user.setRoles(List.of(save.get(i % 2)));
                    userRepository.save(user);
                }

                qudratjon.setPhoneNumber("+998917797278");
                qudratjon.setLastName("Qudratjon");
                qudratjon.setFirstName("Komilov");
                qudratjon.setState(UserState.DEFAULT);
                qudratjon.setTelegramId(747505857L);
                qudratjon.setLanguage(BotAnswerString.uzl);
                qudratjon.setPassword(passwordEncoder.encode("koinot"));
                qudratjon.setUsername("koinot_admin");


                qudratjon.setAddress("Toshkent shahar oybek metro");
                qudratjon.setLatitude(13249847987D);
                qudratjon.setLongitude(49846846814D);
                qudratjon.setBalance(123000D);
                qudratjon.setRoles(save);
                User save1 = userRepository.save(qudratjon);

                ilyosUser.setPhoneNumber("+998900020364");
                ilyosUser.setLastName("Ilyos");
                ilyosUser.setFirstName("Zokhidov");
                ilyosUser.setState(UserState.DEFAULT);
                ilyosUser.setTelegramId(391904237L);
                ilyosUser.setLanguage(BotAnswerString.uzl);
                ilyosUser.setPassword(passwordEncoder.encode("koinot"));
                ilyosUser.setUsername("ilyos_z");

                ilyosUser.setAddress("Toshkent shahar oybek metro");
                ilyosUser.setLatitude(13249847987D);
                ilyosUser.setLongitude(49846846814D);
                ilyosUser.setBalance(123000D);
                ilyosUser.setRoles(save);
                userRepository.save(ilyosUser);

            } else {
                ilyosUser = userRepository.findAllByPhoneNumber("+998900020364");
                qudratjon = userRepository.findAllByPhoneNumber("+998917797278");


            }


        }
    }


}
