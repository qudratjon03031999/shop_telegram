package com.example.shop_bot.service.DBservice;

import com.example.shop_bot.entity.Messages;
import com.example.shop_bot.enums.Msg;
import com.example.shop_bot.repository.MessagesRepository;
import com.example.shop_bot.service.bot.BotAnswerString;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * @className: UserService  $
 * @description: TODO
 * @date: 09 January 2022 $
 * @time: 3:39 AM $
 * @author: Qudratjon Komilov
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class MessageDB {

    private final MessagesRepository messageRepository;

    public Messages saveMessages(Messages message) {

        return messageRepository.save(message);

    }

    public Messages getMessage(String s) {

        return messageRepository.findByName(s);

    }


    public String getMessage(Msg s,String language) {
        try {
            Messages byName=messageRepository.findByName(s.name());

            switch (language) {
                case (BotAnswerString.en): case ("en") : return  byName.getTextEn();
                case (BotAnswerString.ru): case ("ru") : return  byName.getTextRu();
                case (BotAnswerString.uzl): case ("uz") :return  byName.getTextUz();
                default : return "koinot";
            }
        }catch (Exception e){
            return "koinot";
        }


    }

    public Messages getMessage(Msg s) {

        return messageRepository.findByName(s.name());

    }


}
