package com.example.shop_bot.service.bot;

public interface BotAnswerString {

    String time = "time";
    String cancel_client = "cancel_client";
    String koinot = "koinot";
    String KOINOT = "KOINOT";
    String _koinot_prev_ = "_koinot_prev_"; // _koinot_
    String _koinot_next_ = "_koinot_next_"; // _koinot_
    String order = "order";
    String get_order = "get_order";
    String get_time = "get_time";
    String get_month = "get_month";
    String get_stadium = "get_stadium";
    String stadium_name = "✅";
    String verify = "verify";
    String cancelBoss = "cancelBoss";
    String cancelOrder = "cancelOrder"; // _koinot_
    String cancelOrderBTN = "\uD83D\uDDD1"; // _koinot_

    //*************************************//
    String upload_photo = "✅\uD83D\uDC4D";
    String like = "\uD83D\uDC4D";
    String disLice = "\uD83D\uDC4E";
    String LIKE = "\uD83D\uDC4D ";
    String CLOCK = "\uD83D\uDD70 ";
    String NEXT = "➡️";
    String PREV = "⬅️";
    String SUCCESSFUL = "✅";
    String CANCEL = "❌";
    String NOT_FOUND = "❕    \uD83E\uDD37\u200D♂️";
    String MANY = " so'm \uD83D\uDCB8 ";
    String CREATE = "➕";
    String CHANGED = "\uD83D\uDCDD";
    String BACK = "\uD83D\uDD19";
    String GET_PHONE_STICKER = "CAACAgIAAxkBAAIigGBaTp_fA3UfiP7fKogU7SJaAAEO6wAC-QYAAkb7rAQF5n-88vDa9R4E";
    String GET_LOCATION_STICKER = "CAACAgIAAxkBAAIih2BaUO1O53-QWnoDRSXXs630QW3OAAKvAAPEq2gLwrqtVV2a-joeBA";
    String GET_ADDRESS_STICKER = "CAACAgIAAxkBAAIirWBaU3WmgsL3JLuIISmlEaP9ucokAAJcAANH-wkMQa_VBAzsDyceBA";
    String GET_NAME_STICKER = "CAACAgIAAxkBAAIjTWBaWKEbARS2uujp1NELU-YI6ffRAAJQAANH-wkMX9n9KsNqTyUeBA";
    String GET_OPEN_STICKER = "CAACAgIAAxkBAAIjXWBaWhrpZdtPrbz43sCSt-qrwR-5AAJbAANZu_wlZFKnU_pjnh4eBA";
    String GET_CLOSE_STICKER = "CAACAgIAAxkBAAIjXmBaWuOGtayvEyYYN4TpYUoSUfy1AAIkCQACGELuCHBUENT4WrkWHgQ";
    String GET_PHOTO_STICKER = "CAACAgIAAxkBAAIjrWBaZP5K4qkQx28AAbqPHsp-Nk7CGAACGAADTlzSKT5q3R61ronZHgQ";
    String GET_DAYLIGHT_PRICE_STICKER = "CAACAgIAAxkBAAIjYGBaXb8_OqhkpIpBAnivGbcYjYI2AAL8BgACRvusBG1XSZhZ24v-HgQ";
    String GET_NIGHT_PRICE_STICKER = "CAACAgIAAxkBAAIjYWBaXtJWQmMKytyZhQUYP-3VuWykAAJWAANH-wkMEwAB4K3LZ543HgQ";
    String GET_TIME_FOR_CHANGE_PRICE_STICKER = "CAACAgIAAxkBAAIjY2BaYAaut0ex7501Cuuir11FV5_MAAJOAANZu_wlDevP2fnQeCoeBA";
    String GET_SIZE_FOR_STADIUM_PHOTO =
            "AgACAgIAAxkBAAIdA2DtsXjqdSgztPJyLu8K8dquxaOPAAKZtDEb7m1ZS_v9agn7rQbIAQADAgADcwADIAQ";
    String VIDEO_FOR_EXPLAINING_BOT = "BAACAgIAAxkBAAInKWEXzFMnJ8QgNCr229q-pfedoOeWAAKqEAACscTASFdg82STM89rIAQ";
    //    String GET_SIZE_FOR_STADIUM_PHOTO = "https://img3.goodfon.ru/wallpaper/nbig/a/e4/moraine-lake-banff-national-6336.jpg";
    String GET_SUCCESSFUL_STICKER = "CAACAgIAAxkBAAIjZGBaYJfLSIgSvmr5WW6Mbf9-mHxpAAL-AANWnb0K2gRhMC751_8eBA";
    String GET_NEW_LINE_STICKER = System.lineSeparator() + "➖➖➖➖➖➖➖➖➖➖➖➖" + System.lineSeparator() + System.lineSeparator();
    String GET_CLIENT_TIME = System.lineSeparator() + "\uD83D\uDCDD" + System.lineSeparator() + System.lineSeparator();


    //*************************************//
    String CHANGE_LANGUAGE_RU = "язык";
    String CHANGE_LANGUAGE_EN = "language";
    String CHANGE_LANGUAGE_UZL = "til";
    String CHANGE_LANGUAGE_UZ = "тил";

    //**************************************//
    String uzl = "Uz\uD83C\uDDFA\uD83C\uDDFF";
    String uz = "Ўз\uD83C\uDDFA\uD83C\uDDFF";
    String ru = "Ру\uD83C\uDDF7\uD83C\uDDFA";
    String en = "En\uD83C\uDDEC\uD83C\uDDE7";

}
