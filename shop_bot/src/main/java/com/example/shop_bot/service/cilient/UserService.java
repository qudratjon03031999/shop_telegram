package com.example.shop_bot.service.cilient;

import com.example.shop_bot.entity.User;
import com.example.shop_bot.entity.addons.Role;
import com.example.shop_bot.enums.RoleName;
import com.example.shop_bot.payload.ApiResponseModel;
import com.example.shop_bot.payload.DataPageable;
import com.example.shop_bot.payload.ErrorsField;
import com.example.shop_bot.payload.ReqUserData;
import com.example.shop_bot.repository.RoleRepository;
import com.example.shop_bot.repository.UserRepository;
import com.example.shop_bot.service.bot.AttachmentService;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Komilov Qudrtajon
 * @link Telegram Link https://t.me/qudratjon03031999
 * @since 31/01/22
 */
@Service
@Slf4j
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserService {
    UserRepository userRepository;

    RoleRepository roleRepository;

    AttachmentService attachmentService;


    public HttpEntity<?> register(ReqUserData reqSignUp) {

        User user = new User();

        String info = "";

        if (reqSignUp.getId() == null) {
            if (userRepository.existsByPhoneNumber(reqSignUp.getPhoneNumber())) {
                return ResponseEntity.badRequest()
                        .body(
                                new ApiResponseModel(
                                        HttpStatus.CONFLICT.value(),
                                        "field",
                                        List.of(new ErrorsField("phoneNumber", "this phoneNumber is busy"))));
            }
            //vaqtinchalik comment
//            if (reqSignUp.getPassportNumber() !=null && userRepository.existsByPassportNumber(reqSignUp.getPassportNumber())) {
//                return ResponseEntity.badRequest()
//                        .body(
//                                new ApiResponseModel(
//                                        HttpStatus.CONFLICT.value(),
//                                        "field",
//                                        List.of(new ErrorsField("passportNumber", "this passportNumber is busy"))));
//            }
            info = "create";
        } else {
            Optional<User> userOptional = userRepository.findById(reqSignUp.getId());
            if (userOptional.isPresent()) {

                if (userRepository.existsByPhoneNumberAndIdNot(reqSignUp.getPhoneNumber(), reqSignUp.getId())) {
                    return ResponseEntity.badRequest()
                            .body(
                                    new ApiResponseModel(
                                            HttpStatus.CONFLICT.value(),
                                            "field",

                                            List.of(new ErrorsField("phoneNumber", "this phoneNumber is busy"))));
                }
                //vaqtinchalik comment
//                if (userRepository.existsByPassportNumberAndIdNot(reqSignUp.getPassportNumber(), reqSignUp.getId())) {
//                    return ResponseEntity.badRequest()
//                            .body(
//                                    new ApiResponseModel(
//                                            HttpStatus.CONFLICT.value(),
//                                            "field",
//                                            List.of(new ErrorsField("passportNumber", "this passportNumber is busy"))));
//                }
                user = userOptional.get();
            } else {
                return ResponseEntity.badRequest()
                        .body(new ApiResponseModel(HttpStatus.CONFLICT.value(), "not found this id", null));
            }
            info = "edit";
        }

        user.setFirstName(reqSignUp.getFirstName().substring(0,
                1).toUpperCase() + reqSignUp.getFirstName().substring(1));
        user.setLastName(reqSignUp.getLastName().substring(0, 1).toUpperCase() + reqSignUp.getLastName().substring(1));
        user.setMiddleName(reqSignUp.getMiddleName());
        user.setAddress(reqSignUp.getAddress());
        user.setDateOfBirth(reqSignUp.getDateOfBirth());
        user.setPhoneNumber(reqSignUp.getPhoneNumber());
        user.setBalance(reqSignUp.getBalance());
        user.setUsername(reqSignUp.getUsername());
        user.setState(reqSignUp.getState());
        user.setLanguage(reqSignUp.getLanguage());
        user.setAddress(reqSignUp.getAddress());
        user.setLongitude(reqSignUp.getLongitude());
        user.setLatitude(reqSignUp.getLatitude());
        user.setAccountNonExpired(reqSignUp.isAccountNonExpired());
        user.setAccountNonLocked(reqSignUp.isAccountNonLocked());
        user.setCredentialsNonExpired(reqSignUp.isCredentialsNonExpired());
        user.setEnabled(reqSignUp.isEnabled());
        user.setRoles(roleRepository.findAllByNameIn(reqSignUp.getRoles()));
        userRepository.save(user);
        return ResponseEntity.status(HttpStatus.OK)
                .body(new ApiResponseModel(HttpStatus.OK.value(), info, makeReqUser(user)));
    }


    public HttpEntity<?> getPageable(
            Optional<Integer> page,
            Optional<String> sortType, Optional<Integer> size,
            Optional<String> sortBy,
            Optional<String> search,
            RoleName role) {

        Sort.Direction sort = sortType.orElse("DESC").equals("ASC") ? Sort.Direction.ASC : Sort.Direction.DESC;
        try {
            DataPageable userPageable = new DataPageable();
            Page<User> id;

            if (search.isEmpty() || search.get().equals("")) {
                id = userRepository.findByRolesId(roleRepository.findByName(role).getId(), PageRequest.of(
                        page.orElse(0), size.orElse(5), sort, sortBy.orElse("id")));
            } else {

                String s = search.get();
                id = userRepository
                        .findAllByPhoneNumberIgnoreCaseContainingAndFirstNameIgnoreCaseContainingAndLastNameIgnoreCaseContaining(
                                s,
                                s,
                                s,
                                PageRequest.of(
                                        page.orElse(0), size.orElse(5), sort, sortBy.orElse("id")));
            }
            userPageable.setPageable(id.getPageable());
            userPageable.setEmpty(id.isEmpty());
            userPageable.setSort(id.getSort());
            userPageable.setFirst(id.isFirst());
            userPageable.setLast(id.isLast());
            userPageable.setNumber(id.getNumber());
            userPageable.setSize(id.getSize());
            userPageable.setTotalElements(id.getTotalElements());
            userPageable.setTotalPages(id.getTotalPages());
            userPageable.setNumberOfElements(id.getNumberOfElements());
            userPageable.setContent(
                    id.stream()
                            .map(
                                    this::makeReqUser
                            )
                            .collect(Collectors.toList()));
            return ResponseEntity.status(HttpStatus.OK)
                    .body(new ApiResponseModel(HttpStatus.OK.value(), "users", userPageable));
        } catch (Exception e) {
            return ResponseEntity.badRequest()
                    .body(
                            new ApiResponseModel(
                                    HttpStatus.CONFLICT.value(),
                                    "The request is incorrect",
                                    List.of(new ErrorsField("error", e.getMessage()))));
        }
    }


    public HttpEntity<?> getUserById(Long id) {
        try {
            Optional<User> byId = userRepository.findById(id);
            if (byId.isPresent()) {
                User user = byId.get();
                return ResponseEntity.status(HttpStatus.OK)
                        .body(
                                new ApiResponseModel(
                                        HttpStatus.OK.value(),
                                        "user",
                                        makeReqUser(user)));
            }
            return ResponseEntity.badRequest()
                    .body(
                            new ApiResponseModel(
                                    HttpStatus.CONFLICT.value(),
                                    "not found user",
                                    List.of(new ErrorsField("error", null))));
        } catch (Exception e) {
            return ResponseEntity.badRequest()
                    .body(
                            new ApiResponseModel(
                                    HttpStatus.CONFLICT.value(),
                                    "The request is incorrect",
                                    List.of(new ErrorsField("error", e.getMessage()))));
        }
    }

    public ReqUserData makeReqUser(User user) {
        return new ReqUserData(
                user.getId(),
                user.getPhoneNumber(),
                user.getIpAddress(),
                user.getDateOfBirth(),
                user.getEmail(),
                user.getExpiredCode(),
                user.getVerifyCode(),
                user.getUsername(),
                user.getGender(),
                user.getBalance(),
                user.getTelegramId(),
                user.getFirstName(),
                user.getLastName(),
                user.getMiddleName(),
                user.getState(),
                user.getLanguage(),
                user.getAddress(),
                user.getFamily(),
                user.getLongitude(),
                user.getLatitude(),
                user.getPhoto() == null ? null : attachmentService.generateFile(user.getPhoto()),
                user.getRoles().stream().map(Role::getName).collect(
                        Collectors.toList()),
                user.getPassportNumber(),
                user.isAccountNonExpired(),
                user.isAccountNonLocked(),
                user.isCredentialsNonExpired(),
                user.isEnabled()
        );
    }
}
