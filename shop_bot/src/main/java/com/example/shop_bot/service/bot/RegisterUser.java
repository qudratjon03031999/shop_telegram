package com.example.shop_bot.service.bot;

import com.example.shop_bot.entity.Messages;
import com.example.shop_bot.entity.User;
import com.example.shop_bot.enums.Msg;
import com.example.shop_bot.enums.UserState;
import com.example.shop_bot.exception.ExceptionSend;
import com.example.shop_bot.service.DBservice.MessageDB;
import com.example.shop_bot.service.DBservice.UserDB;
import com.example.shop_bot.telegramBot.Bot;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.ParseMode;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;

/**
 * @className: RegisterUser  $
 * @description: TODO
 * @date: 09 January 2022 $
 * @time: 3:45 AM $
 * @author: Qudratjon Komilov
 */
@Service
@Slf4j
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RegisterUser {

   @Autowired
   ExceptionSend exceptionSend;
   @Autowired UserDB userDB;
   @Autowired Bot bot;
   @Autowired WorkingALot workingALot;
   @Autowired MessageDB messageDB;


    public void userLanguage(Update update) throws TelegramApiException {
        long chat_id = update.getMessage().getChatId();

        if (userDB.existsByTelegramId(chat_id)) {
            User user = userDB.findAllByTelegramId(chat_id);
            if (user.getLanguage() == null) {
                user.setState(UserState.CHOOSE_LANGUAGE);
                userDB.saveUser(user);
                getLanguage(update.getMessage().getFrom().getLanguageCode(), chat_id);
            }
            user.setState(UserState.DEFAULT);
            userDB.saveUser(user);
            defaultMessage(chat_id, user.getLanguage());

        } else {

            User user = new User();
            user.setTelegramId(chat_id);
            user.setFirstName(update.getMessage().getFrom().getFirstName());
            user.setLastName(update.getMessage().getFrom().getLastName());
            user.setState(UserState.CHOOSE_LANGUAGE);
            user.setUsername(update.getMessage().getFrom().getUserName());
            userDB.saveUser(user);
            getLanguage(update.getMessage().getFrom().getLanguageCode(), chat_id);
        }
    }

    public void editLanguage(Update update) {
        int message_id = update.getCallbackQuery().getMessage().getMessageId();
        long chat_id = update.getCallbackQuery().getMessage().getChatId();
        try {
            Long id = update.getCallbackQuery().getFrom().getId();
            User user = userDB.findAllByTelegramId(id);
            String s = update.getCallbackQuery().getData();
            user.setLanguage(s);
            user.setState(UserState.DEFAULT);
            userDB.saveUser(user);

//            send(update.getMessage().getChatId(),messageDB.getMessage(Msg.HELLO).getTextEn());


        } catch (Exception e) {
            exceptionSend.senException("edit language => ", e, null);
            log.error("edit language => ", e);
        }
    }

    public void sendVideo(Update update) {
//        try{
//            bot.execute( new SendVideo().setChatId( update.getMessage().getChatId() )
//                    .setVideo( BotAnswerString.VIDEO_FOR_EXPLAINING_BOT ) );
//        }catch(TelegramApiException e){
//            e.printStackTrace();
//        }
    }

    public void getLanguage(String language, long chat_id) {
        try {
            List<String> strings = new ArrayList<>();
            strings.add(BotAnswerString.en);
            strings.add(BotAnswerString.ru);
            strings.add(BotAnswerString.uzl);
            Messages message = messageDB.getMessage(Msg.HELLO_CHOOSE_A_LANGUAGE);

            if (language == null) {
                bot.execute(workingALot.inline(chat_id,
                        strings,
                        message.getTextEn())); // Sending our message object to user
            } else {
                bot.execute(workingALot.inline(chat_id,
                        strings,
                        messageDB.getMessage(Msg.HELLO_CHOOSE_A_LANGUAGE,
                                language))); // Sending our message object to user

            }
        } catch (Exception e) {
            exceptionSend.senException("get language  => ", e, null);
            log.error("get language  => ", e);
        }

    }

    public void defaultMessage(Long id, String language) throws TelegramApiException {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(String.valueOf(id));
        sendMessage.setParseMode(ParseMode.HTML);
        sendMessage.setText(messageDB.getMessage(Msg.SUCCESSES, language));
        bot.execute(sendMessage);

    }


}
