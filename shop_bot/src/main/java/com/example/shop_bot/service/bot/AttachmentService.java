package com.example.shop_bot.service.bot;


import com.example.shop_bot.entity.User;
import com.example.shop_bot.entity.addons.Attachment;
import com.example.shop_bot.enums.AttachmentTypeEnumWhy;
import com.example.shop_bot.enums.QualityType;
import com.example.shop_bot.payload.ApiResponseModel;
import com.example.shop_bot.payload.ResUploadFile;
import com.example.shop_bot.repository.AttachementRepository;
import com.example.shop_bot.repository.UserRepository;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import net.coobird.thumbnailator.Thumbnails;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.StreamUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.lang.module.ResolutionException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

@Component
@Slf4j
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AttachmentService {

    @Value("${upload.folder}")
    private static String path;
    AttachementRepository attachmentRepository;
    UserRepository userRepository;

    public HttpEntity<ApiResponseModel> userProfile(MultipartHttpServletRequest request, User user, String name) {
        try {
            Iterator<String> iterator = request.getFileNames();
            MultipartFile multipartFile;
            List<ResUploadFile> resUploadFiles = new ArrayList<>();
            if (iterator.hasNext()) {
                multipartFile = request.getFile(iterator.next());
                assert multipartFile != null;
                Attachment attachment = saveMultipartFile(multipartFile, user, name, AttachmentTypeEnumWhy.PROFILE);
                resUploadFiles.add(generateFile(attachment));
                user.setPhoto(attachment);
                userRepository.save(user);
            }
            return ResponseEntity.status(HttpStatus.OK)
                    .body(new ApiResponseModel(HttpStatus.OK.value(), "saved", resUploadFiles));
        } catch (Exception e) {
            log.error(" controller uploadPhotoFileList => ", e);
            return ResponseEntity.status(HttpStatus.CONFLICT)
                    .body(new ApiResponseModel(HttpStatus.CONFLICT.value(), "error", e.getMessage()));
        }
    }

    public Attachment saveMultipartFile(MultipartFile multipartFile, User user, String name, AttachmentTypeEnumWhy why) {

        Attachment photo = new Attachment();
        photo.setSize(multipartFile.getSize());
        photo.setUser(user);
        photo.setName(name == null || name.equals("null") ? multipartFile.getOriginalFilename() : name);
        photo.setContentType(multipartFile.getContentType());
        photo.setWhy(why);
        if (getExt(multipartFile.getOriginalFilename()) == null || multipartFile.getSize() == 0) {
            return null;
        }

        photo.setExtension(getExt(multipartFile.getOriginalFilename()));

        Attachment save = attachmentRepository.save(photo);

        Calendar calendar = new GregorianCalendar();

        File uploadFolder = new File(
                path + "/" + QualityType.ORIGINAL + "/" + calendar.get(Calendar.YEAR) + "/" + (calendar.get(
                        Calendar.MONTH) + 1) + "/" +
                        calendar.get(Calendar.DAY_OF_MONTH));

        if (uploadFolder.mkdirs() && uploadFolder.exists()) {
            log.info("create new folder ==> " + uploadFolder.getAbsolutePath());
        }

        uploadFolder = uploadFolder.getAbsoluteFile();
        File file = new File(uploadFolder + "/" + save.getId() + "_" + save.getName());
        save.setPathOriginal(file.getAbsolutePath());

        try {
            multipartFile.transferTo(file);
            Attachment save1 = attachmentRepository.save(save);

            new Thread(() -> {
                imageFormat(save1, file);
            }).start();
            return save1;

        } catch (Exception e) {
            attachmentRepository.delete(save);
            log.error(" controller uploadPhotoFileList => ", e);
            return null;
        }

    }

    public Attachment saveMultipartFileVideo(MultipartFile multipartFile, User user, String name, AttachmentTypeEnumWhy why) {

        Attachment photo = new Attachment();
        photo.setSize(multipartFile.getSize());
        photo.setUser(user);
        photo.setName(name == null || name.equals("null") ? multipartFile.getOriginalFilename() : name);
        photo.setContentType(multipartFile.getContentType());
        photo.setWhy(why);
        if (getExt(multipartFile.getOriginalFilename()) == null || multipartFile.getSize() == 0) {
            return null;
        }

        photo.setExtension(getExt(multipartFile.getOriginalFilename()));

        Attachment save = attachmentRepository.save(photo);

        Calendar calendar = new GregorianCalendar();

        File uploadFolder = new File(
                path + "/" + QualityType.ORIGINAL + "/" + calendar.get(Calendar.YEAR) + "/" + (calendar.get(
                        Calendar.MONTH) + 1) + "/" +
                        calendar.get(Calendar.DAY_OF_MONTH));

        if (uploadFolder.mkdirs() && uploadFolder.exists()) {
            log.info("create new folder ==> " + uploadFolder.getAbsolutePath());
        }

        uploadFolder = uploadFolder.getAbsoluteFile();
        File file = new File(uploadFolder + "/" + save.getId() + "_" + save.getName());
        save.setPathOriginal(file.getAbsolutePath());

        try {
            multipartFile.transferTo(file);
            return attachmentRepository.save(save);

        } catch (Exception e) {
            attachmentRepository.delete(save);
            log.error(" controller uploadPhotoFileList => ", e);
            return null;
        }
    }

    public void imageFormat(Attachment attachment, File file) {
        attachment.setPathAverage(resize(file.getAbsolutePath(), QualityType.AVERAGE, 0.7f));
        attachment.setPathLow(resize(file.getAbsolutePath(), QualityType.LOW, 0.3f));
        attachmentRepository.save(attachment);

    }

    public String getExt(String fileName) {
        String ext = null;
        if (fileName != null && !fileName.isEmpty()) {
            int dot = fileName.lastIndexOf(".");
            if (dot > 0 && dot <= fileName.length() - 2) {
                ext = fileName.substring(dot);
            }
        }
        return ext;
    }

    public ResUploadFile generateFile(Attachment attachment) {
        try {
            return new ResUploadFile(attachment.getId(),
                    attachment.getName(),
                    attachment.getWhy(),
                    attachment.getContentType(),
                    attachment.getSize(),
                    ServletUriComponentsBuilder.fromCurrentContextPath()
                            .path("/koinot/attachment/v1/byteFile/")
                            .path(attachment.getId()
                                    .toString())
                            .toUriString());
        } catch (Exception e) {
            return null;
        }

    }

    public void deletePhoto(List<Attachment> p) {
        for (Attachment attachment : p) {
            new File(attachment.getPathOriginal()).delete();
            new File(attachment.getPathAverage()).delete();
            new File(attachment.getPathLow()).delete();
        }
        attachmentRepository.deleteAll(p);
    }

    public String resize(String pathFile, QualityType qualityType, float quality) {
        try {
            File file = new File(pathFile);
            BufferedImage read = ImageIO.read(file);
            Calendar calendar = new GregorianCalendar();
            String fileP = path + "/" + qualityType + "/" + calendar.get(Calendar.YEAR) + "/" +
                    (calendar.get(Calendar.MONTH) + 1) + "/" + calendar.get(Calendar.DAY_OF_MONTH) + "/";
            File file1 = new File(fileP + read.getWidth() + "_" + read.getHeight() + "_" + quality + file.getName());

            if (file.exists() && file1.isFile()) {
                return file1.getAbsolutePath();
            }
            File uploadFolder = new File(fileP);
            if (uploadFolder.mkdirs() && uploadFolder.exists()) {
                log.info("create new folder ==> " + uploadFolder.getAbsolutePath());
            }
            BufferedImage bufferedImage = resizeImage(ImageIO.read(file), read.getWidth(), read.getHeight(), quality);
            ImageIO.write(bufferedImage, "jpg", file1);
            return file1.getAbsolutePath();
        } catch (Exception e) {
            log.error("error resize image", e);
            return pathFile;
        }
    }

    public BufferedImage resizeImage(BufferedImage originalImage, int targetWidth, int targetHeight, float quality) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        Thumbnails.of(originalImage)
                .size((int) (targetWidth * quality), (int) (targetHeight * quality))
                .outputFormat("JPEG")
                .outputQuality(1)
                .toOutputStream(outputStream);
        byte[] data = outputStream.toByteArray();
        ByteArrayInputStream inputStream = new ByteArrayInputStream(data);
        return ImageIO.read(inputStream);
    }

    public HttpEntity<?> byteFileQuality(Long id, QualityType type) throws IOException {

        try {
            Attachment one = attachmentRepository.findById(id)
                    .orElseThrow(() -> new ResolutionException("getAttachmentID"));

            switch (type) {
                case AVERAGE:
                    return ResponseEntity.ok()
                            .contentType(MediaType.parseMediaType(one.getContentType()))
                            .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + one.getName() + "\"")
                            .body(Files.readAllBytes(Paths.get(one.getPathAverage())));
                case LOW:
                    return ResponseEntity.ok()
                            .contentType(MediaType.parseMediaType(one.getContentType()))
                            .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + one.getName() + "\"")
                            .body(Files.readAllBytes(Paths.get(one.getPathLow())));
                default:
                    return ResponseEntity.ok()
                            .contentType(MediaType.parseMediaType(one.getContentType()))
                            .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + one.getName() + "\"")
                            .body(Files.readAllBytes(Paths.get(one.getPathOriginal())));
            }

        } catch (Exception e) {

            return ResponseEntity.ok()
                    .contentType(MediaType.IMAGE_JPEG)
                    .body(StreamUtils.copyToByteArray(new ClassPathResource("image/koinot.jpg").getInputStream()));

        }

    }

}
