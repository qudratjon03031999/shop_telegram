package com.example.shop_bot.telegramBot;


import com.example.shop_bot.service.bot.GetUpdate;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.util.List;

@Component
@Slf4j
@RequiredArgsConstructor
public class Bot extends TelegramLongPollingBot {

//    562505195

    private final GetUpdate getUpdate;
    @Value("${bot.token}")
    private String botToken;
    @Value("${bot.name}")
    private String botUsername;

    @Override
    public String getBotUsername() {
        return botUsername;
    }

    @Override
    public String getBotToken() {
        return botToken;
    }

    @SneakyThrows
    @Override
    public void onUpdateReceived(Update update) {
        log.error("bot  AnswerBot=> ", update.getMessage().getText());
        getUpdate.answerBot(update);
    }

    @SneakyThrows
    @Override
    public void onUpdatesReceived(List<Update> updates) {
        for (Update update : updates) {
            try {
                getUpdate.answerBot(update);
            } catch (Exception e) {
                log.error("bot  AnswerBot=> ", e);
            }
        }
    }


}
