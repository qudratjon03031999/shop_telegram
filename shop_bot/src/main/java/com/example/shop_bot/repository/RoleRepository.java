package com.example.shop_bot.repository;

import com.example.shop_bot.entity.addons.Role;
import com.example.shop_bot.enums.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {

    List<Role> findAllByName(RoleName name);

    Role findByName(RoleName name);


    List<Role> findAllByNameIn(List<RoleName> names);


}
