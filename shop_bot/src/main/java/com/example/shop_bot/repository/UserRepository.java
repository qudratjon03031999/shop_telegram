package com.example.shop_bot.repository;

import com.example.shop_bot.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author Komilov Qudrtajon
 * @link Telegram Link https://t.me/qudratjon03031999
 * @since 26/08/21
 */


@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    boolean existsByTelegramId(Long telegramId);

    User findAllByTelegramId(Long telegramId);

    Page<User> findByRolesId(Integer roles_id, Pageable pageable);

    Optional<User> findByPhoneNumber(String phoneNumber);


    boolean existsByPhoneNumberAndIdNot(String phoneNumber, Long id);

    Page<User> findAllByPhoneNumberIgnoreCaseContainingAndFirstNameIgnoreCaseContainingAndLastNameIgnoreCaseContaining(String phoneNumber,
                                                                                                                       String firstName, String lastName,
                                                                                                                       Pageable pageable);

    boolean existsAllByPhoneNumber(String phoneNumber);

    User findAllByPhoneNumber(String phoneNumber);

    boolean existsByEmail(String email);

    boolean existsByEmailAndIdNot(String email, Long id);

    boolean existsByPassportNumber(String passportNumber);

    boolean existsByPassportNumberAndIdNot(String passportNumber, Long id);

    boolean existsByPhoneNumber(String phoneNumber);
}
