package com.example.shop_bot.entity;

import com.example.shop_bot.entity.addons.Attachment;
import com.example.shop_bot.entity.addons.Role;
import com.example.shop_bot.enums.Family;
import com.example.shop_bot.enums.Gender;
import com.example.shop_bot.enums.UserState;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * @author Komilov Qudrtajon
 * @link Telegram Link https://t.me/qudratjon03031999
 * @since 26/08/21
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "Users")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class User implements UserDetails {

    @Id
    @SequenceGenerator(
            name = "users_id_sequence",
            sequenceName = "users_id_sequence"
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "users_id_sequence"
    )
    Long id;

    @Column(unique = true)
    String phoneNumber; // qo'lda kiritadigan tel nomeri

    @JsonIgnore
    String password; // paroli

    String ipAddress; // ip adresi

    Date dateOfBirth; // tug'lgan kuni ishlatilmagan

    String email; // emaili har ixtimolga qarshi

    @JsonIgnore
    Date expiredCode;


    @JsonIgnore
    String verifyCode; // android uchun code

    String username; //  bot uchun ishlatmoqchi edim

    @Enumerated(EnumType.STRING)
    Gender gender; // jinsi

    Double balance; // hisob raqamidagi puli

    @Column(unique = true)
    Long telegramId;

    String firstName;

    String lastName;

    String middleName;

    @Enumerated(EnumType.STRING)
    UserState state; // bot uchun holatlari

    String language; // tili

    String address;

    @Enumerated(EnumType.STRING)
    Family family;

    Double longitude; // userni hozir tirgan joyi

    Double latitude; // userni hozir tirgan joyi

    @OneToOne
    Attachment photo;

    @ManyToMany(fetch = FetchType.EAGER)
    @JsonIgnore
    @JoinTable(name = "user_role", joinColumns = {@JoinColumn(name = "user_id")}, inverseJoinColumns = {@JoinColumn(name = "role_id")})
    List<Role> roles; // userni roli

    @Column
    String passportNumber;

    @JsonIgnore
    boolean accountNonExpired = true;

    @JsonIgnore
    boolean accountNonLocked = true;

    @JsonIgnore
    boolean credentialsNonExpired = true;

    @JsonIgnore
    boolean enabled = true;

    @Override
    public String getUsername() {
        return this.phoneNumber;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.roles;
    }

    @Override
    public boolean isAccountNonExpired() {
        return this.accountNonExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return this.accountNonLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return this.credentialsNonExpired;
    }

    @Override
    public boolean isEnabled() {
        return this.enabled;
    }

}
