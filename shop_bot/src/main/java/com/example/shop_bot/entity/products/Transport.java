package com.example.shop_bot.entity.products;

import com.example.shop_bot.entity.addons.AddOptions;
import com.example.shop_bot.entity.addons.Models;
import com.example.shop_bot.entity.products.template.ReadyProduct;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * @className: BotMessage  $
 * @description: TODO
 * @date: 09 January 2022 $
 * @time: 6:35 AM $
 * @author: Qudratjon Komilov
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "transport")
public class Transport extends ReadyProduct {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // Model

    @ManyToOne
    private Models models;


    //  Kuzov

    @ManyToOne
    private AddOptions transportTypeOfBody;


    // Ishlab chiqarilgan yili

    private Date yearOfIssue;


    // Probeg

    private Double mileage;


    // transmissiya quttisi
    @ManyToOne
    private AddOptions typeOfTransmission;

    //  Moshina rangi
    private String colour;

    //  search/post by color code
    private String colourCode;

    //  dvigatel hajmi
    private Double engineCapacity;


    //  yoqilg'i turi
    private String typeOfFuel;


    // haydovchi birligi - > privod
    private String driveUnit;

    // tashqaridagi bor qulayliklar
    @ManyToMany
    private List<AddOptions> hasTransportOptionsOutside;

    // moshinada bor qulayliklar
    @ManyToMany
    private List<AddOptions> hasTransportOptions;

    // moshina salon
    @ManyToMany
    private List<AddOptions> hasTransportInteriorOptions;

    // moshina media qulayliklari
    @ManyToMany
    private List<AddOptions> hasTransportMediaOptions;


    // moshina optikasi
    @ManyToMany
    private List<AddOptions> hasTransportOpticsOptions;

    // qoshimcha
    @ManyToMany
    private List<AddOptions> additionally;

    /*
     *
     * sotib olish opsiyasi bilan ijaraga
     * if false - sotib olish opsiyasi yoq
     * if true -  sotib olish opsiyasi bor
     *
     * */

    private Boolean hasLeasing = false;

    /******** Mototexnika (Moto technics) ********/

    // Transport texnika turi

    @ManyToOne
    private AddOptions typeOfMotoTechnics;

    /******** Suv Transporti (Water Transport) ********/

    // suv texnika turi

    @ManyToOne
    private AddOptions typeOfWaterTechnics;

    /******** Tijorat Transport Turi (Commercial Type of Transport) ********/

    // Tijorat Transport Categoriyasi (gruzovik, avtobus ...)

    @ManyToOne
    private AddOptions commercialTransportCategory;

    // avtobus turi

    private String typeOfBus;

    // Avtobus joylar soni

    private Double numberOfBusSeats;

    /******** Zapchastlari (Spares) ********/

    // zapchast nomi

    private String nameOfSpares;

    /*
     *
     * Zapchast mavjudligi/buyurtma bo'yicha
     * if false = buyurtma bo'yicha
     * if true = mavjud
     *
     * */
    private Boolean availability = false;

    /******** Transport Ijara (Transport Rent) ********/

    // categoriya tanlash
    @ManyToOne
    private AddOptions chooseCategoryForRent;

    /******** Transport Remont va Xizmatlar  (Repairs and Services) ********/

    @ManyToMany
    private List<AddOptions> repairsAndServices;


}
