package com.example.shop_bot.entity.addons;

import com.example.shop_bot.enums.RoleName;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Role implements GrantedAuthority {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    @Column(unique = true)
    @Enumerated(EnumType.STRING)
    RoleName name;


    @Override
    public String getAuthority() {
        return name.name();
    }


}
