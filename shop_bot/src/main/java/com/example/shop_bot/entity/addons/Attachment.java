package com.example.shop_bot.entity.addons;

import com.example.shop_bot.entity.User;
import com.example.shop_bot.enums.AttachmentTypeEnumWhy;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * @className: BotMessage  $
 * @description: TODO
 * @date: 09 January 2022 $
 * @time: 6:35 AM $
 * @author: Qudratjon Komilov
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "attachment")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Attachment {

    @Id
    @SequenceGenerator(
            name = "attachment_id_sequence",
            sequenceName = "attachment_id_sequence"
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "attachment_id_sequence"
    )
    Long id;

    @OrderBy
    @CreationTimestamp
    @Column(nullable = false, updatable = false)
    Timestamp createdAt;

    @UpdateTimestamp
    @Column(nullable = false)
    Timestamp updatedAt;

    @CreatedBy
    @Column(updatable = false)
    Long createdBy;

    @LastModifiedBy
    Long updatedBy;

    @Column(columnDefinition = "TEXT", nullable = false)
    String name;

    String urlTelegram;

    long size;

    String contentType;

    String photo;

    String pathOriginal;

    String pathAverage;

    String pathLow;

    String extension;

    @ManyToOne
    User user;

    @Enumerated(value = EnumType.STRING)
    AttachmentTypeEnumWhy why;


}
