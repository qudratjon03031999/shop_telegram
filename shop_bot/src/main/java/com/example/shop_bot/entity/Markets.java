package com.example.shop_bot.entity;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * @className: BotMessage  $
 * @description: TODO
 * @date: 09 January 2022 $
 * @time: 6:35 AM $
 * @author: Qudratjon Komilov
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "Markets")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Markets {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @OrderBy
    @CreationTimestamp
    @Column(nullable = false, updatable = false)
    Timestamp createdAt;

    @UpdateTimestamp
    @Column(nullable = false)
    Timestamp updatedAt;

    @CreatedBy
    @Column(updatable = false)
    Long createdBy;

    @LastModifiedBy
    Long updatedBy;

    @Column(nullable = false, unique = true)
    String name;

    @Column(nullable = false)
    String description;


}
