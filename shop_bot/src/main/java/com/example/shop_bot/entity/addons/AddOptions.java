package com.example.shop_bot.entity.addons;

import com.example.shop_bot.enums.Futures;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * @className: BotMessage  $
 * @description: TODO
 * @date: 09 January 2022 $
 * @time: 6:35 AM $
 * @author: Qudratjon Komilov
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "addOptions")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AddOptions {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;


    @UpdateTimestamp
    @Column(nullable = false)
    Timestamp updatedAt;

    @CreatedBy
    @Column(updatable = false)
    Long createdBy;

    @LastModifiedBy
    Long updatedBy;

    @Column(columnDefinition = "TEXT", nullable = false)
    String descriptionUz;//default use

    @Column(columnDefinition = "TEXT", nullable = false)
    String descriptionRu;

    @Column(columnDefinition = "TEXT", nullable = false)
    String descriptionEn;

    @Column(nullable = false)
    String nameUz;//default use

    @Column(nullable = false)
    String nameRu;

    @Column(nullable = false)
    String nameEn;

    @Column(nullable = false)
    String icon;

    @ManyToOne
    Attachment image;

    @Enumerated(EnumType.STRING)
    Futures futures;


}
