package com.example.shop_bot.entity.addons;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * @className: Brands  $
 * @description: TODO
 * @date: 15 January 2022 $
 * @time: 12:42 PM $
 * @author: Qudratjon Komilov
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "Models")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Models {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @OrderBy
    @CreationTimestamp
    @Column(nullable = false, updatable = false)
    Timestamp createdAt;

    @UpdateTimestamp
    @Column(nullable = false)
    Timestamp updatedAt;

    @CreatedBy
    @Column(updatable = false)
    Long createdBy;

    @LastModifiedBy
    Long updatedBy;

    @Column(columnDefinition = "TEXT", nullable = false)
    String descriptionUz;//default use

    @Column(columnDefinition = "TEXT", nullable = false)
    String descriptionRu;

    @Column(columnDefinition = "TEXT", nullable = false)
    String descriptionEn;

    @Column(nullable = false)
    String name;

    @ManyToOne
    Brands brands;
}
