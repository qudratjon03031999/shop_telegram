package com.example.shop_bot.controller;


import com.example.shop_bot.entity.User;
import com.example.shop_bot.enums.QualityType;
import com.example.shop_bot.payload.ApiResponseModel;
import com.example.shop_bot.sercurity.CurrentUser;
import com.example.shop_bot.service.bot.AttachmentService;
import io.swagger.annotations.ApiOperation;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import springfox.documentation.annotations.ApiIgnore;

import java.io.IOException;

@Slf4j
@RestController
@RequestMapping("/koinot/attachment/v1")
@RequiredArgsConstructor
public class AttachmentController {

    private final AttachmentService attachmentService;

    @PostMapping("/userProfile")
    public HttpEntity<ApiResponseModel> userProfile(
            MultipartHttpServletRequest request,
            @ApiIgnore @CurrentUser User user, @RequestParam(required = false, defaultValue = "null") String name) {
        return attachmentService.userProfile(request, user, name);
    }


    @GetMapping("/byteFile/{id}")
    @ApiOperation("QualityType values: ORIGINAL, AVERAGE, LOW")
    public HttpEntity<?> byteFileQuality(
            @PathVariable(required = false) Long id,
            @RequestParam(required = false, defaultValue = "ORIGINAL") QualityType type) throws IOException {
        return attachmentService.byteFileQuality(id, type);
    }


    @ExceptionHandler
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public HttpEntity<ApiResponseModel> handleException(@ApiIgnore @CurrentUser User user, Exception e) {
        log.error(" AuthController handleException => ", e);
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new ApiResponseModel(HttpStatus.BAD_REQUEST.value(), e.getMessage(), e.hashCode()));
    }
}
