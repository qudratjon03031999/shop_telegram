package com.example.shop_bot.exception;

public class NotUpdatedException extends RuntimeException{
    public NotUpdatedException() {
        super();
    }

    public NotUpdatedException(String message) {
        super(message);
    }

    public NotUpdatedException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotUpdatedException(Throwable cause) {
        super(cause);
    }

    protected NotUpdatedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
