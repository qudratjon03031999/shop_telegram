package com.example.shop_bot.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import java.time.LocalDateTime;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ExceptionResponse> handleNotFoundException(
            final NotFoundException e, WebRequest webRequest
    ){
        return exceptionResponseResponse(e, webRequest, HttpStatus.NOT_FOUND);
    }
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(NotUpdatedException.class)
    public ResponseEntity<ExceptionResponse> handleNotUpdatedException(
            final NotUpdatedException e, WebRequest webRequest
    ){
        return exceptionResponseResponse(e, webRequest, HttpStatus.BAD_REQUEST);
    }


    private ResponseEntity<ExceptionResponse> exceptionResponseResponse(
            final Exception e, final WebRequest webRequest, final HttpStatus status
            ){
        return new ResponseEntity<>(
                ExceptionResponse.builder()
                        .massage(e.getMessage())
                        .occurredAt(LocalDateTime.now())
                        .status(status.value())
                        .build(), status
        );
    }
}
