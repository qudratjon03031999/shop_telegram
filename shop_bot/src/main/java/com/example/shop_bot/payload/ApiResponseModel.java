package com.example.shop_bot.payload;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.FieldDefaults;

/**
 * @description: TODO
 * @projectName koinot_market
 * @date: 15 February 2022 $
 * @time: 22:59 $
 * @author: Qudratjon Komilov
 */

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
@ApiModel("ApiResponseModel")
public class ApiResponseModel {
    @ApiModelProperty(name = "success code", position = 1, example = "200")
    int success;
    @ApiModelProperty(name = "response message", position = 2, example = "successes")
    String message;
    @ApiModelProperty(name = "response object", position = 3, example = "null")
    Object objectKoinot;

    public ApiResponseModel(int success, String message) {
        this.success = success;
        this.message = message;
    }
}
