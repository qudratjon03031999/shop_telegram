package com.example.shop_bot.payload;

import com.example.shop_bot.enums.AttachmentTypeEnumWhy;
import lombok.*;
import lombok.experimental.FieldDefaults;

/**
 * @description: TODO
 * @projectName koinot_market
 * @date: 15 February 2022 $
 * @time: 22:59 $
 * @author: Qudratjon Komilov
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ResUploadFile {
     Long fileId;
     String fileName;
     AttachmentTypeEnumWhy why;
     String fileType;
     long size;
     String link;
}
