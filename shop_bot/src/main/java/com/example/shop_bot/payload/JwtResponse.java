package com.example.shop_bot.payload;

import com.example.shop_bot.enums.RoleName;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

/**
 * @description: TODO
 * @projectName koinot_market
 * @date: 15 February 2022 $
 * @time: 22:59 $
 * @author: Qudratjon Komilov
 */

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
public class JwtResponse {
    String accessToken;
    String tokenType = "Bearer";
    String username;
    String lastName;
    String firstName;
    List<RoleName> roles;

    public JwtResponse(String accessToken, String username, String lastName, String firstName, List<RoleName> roles) {
        this.accessToken = accessToken;
        this.username = username;
        this.lastName = lastName;
        this.firstName = firstName;
        this.roles = roles;
    }
}
