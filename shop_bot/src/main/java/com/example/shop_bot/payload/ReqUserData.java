package com.example.shop_bot.payload;

import com.example.shop_bot.enums.Family;
import com.example.shop_bot.enums.Gender;
import com.example.shop_bot.enums.RoleName;
import com.example.shop_bot.enums.UserState;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.Pattern;
import java.util.Date;
import java.util.List;

/**
 * @description: TODO
 * @projectName koinot_market
 * @date: 15 February 2022 $
 * @time: 22:59 $
 * @author: Qudratjon Komilov
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ReqUserData {

 Long id;

 @Pattern(regexp = "^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\\\\\\\\s\\\\\\\\./0-9]*$", message = "telephone number is invalid")
 @ApiModelProperty(notes = "phone number ", name = "phoneNumber", required = true, value = "+998917797278")
 String phoneNumber;//

 String ipAddress; //

 Date dateOfBirth; //

 String email; //

 Date expiredCode;//

 String verifyCode; //

 String username; //

 Gender gender; //

 Double balance; //

 Long telegramId;//

 String firstName;//

 String lastName;//

 String middleName;//

 UserState state;//

 String language; //

 @ApiModelProperty(notes = "address", name = "address", required = true, value = "Buxoro")
 String address;//

 Family family;//

 @Pattern(regexp = "^(-?\\d+(\\.\\d+)?)$", message = "longitude is invalid")
 Double longitude; //

 @Pattern(regexp = "^(-?\\d+(\\.\\d+)?)$", message = "latitude is invalid")
 Double latitude;//

 ResUploadFile photo;//

 List<RoleName> roles; //

 String passportNumber;//

 boolean accountNonExpired;

 boolean accountNonLocked;

 boolean credentialsNonExpired;

 boolean enabled;

}
