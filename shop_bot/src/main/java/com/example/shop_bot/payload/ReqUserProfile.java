package com.example.shop_bot.payload;

import com.example.shop_bot.enums.Family;
import com.example.shop_bot.enums.Gender;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.List;

/**
 * @description: TODO
 * @projectName koinot_market
 * @date: 15 February 2022 $
 * @time: 22:59 $
 * @author: Qudratjon Komilov
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ReqUserProfile {

    Integer id;

    @NotNull
    @NotBlank()
    @Pattern(regexp = "^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\\\\\\\\s\\\\\\\\./0-9]*$", message = "telephone number is invalid")
    @ApiModelProperty(notes = "phone number ", name = "phoneNumber", required = true, value = "+998917797278")
    String phoneNumber;

    @NotNull(message = "password must be not null")
    @NotBlank(message = "password is empty")
    @Length(min = 4, max = 30, message = "password is invalid length is min = 4 max = 30")
    @ApiModelProperty(notes = "password", name = "password", required = true, value = "123pas")
    String password;

    @Pattern(regexp = "^\\d\\d\\d\\d-(0?[1-9]|1[0-2])-(0?[1-9]|[12][0-9]|3[01])$", message = "date Of Birthis invalid")
    @ApiModelProperty(notes = "date of birth", name = "date of birth", required = true, value = "1999-03-03")
    String dateOfBirth;

    @ApiModelProperty(notes = "email", name = "email", required = true, value = "komilovqudratjon@gmail.com")
    @Email()
    String email;

    Gender gender;

    Family family;

    @Pattern(regexp = "^(-?\\d+(\\.\\d+)?)$", message = "longitude is invalid")
    double longitude;

    @Pattern(regexp = "^(-?\\d+(\\.\\d+)?)$", message = "latitude is invalid")
    double latitude;

    @ApiModelProperty(notes = "address", name = "address", required = true, value = "Buxoro")
    String address;

    @ApiModelProperty(notes = "firstName", name = "firstName", required = true, value = "Qudratjon")
    String firstName;

    @ApiModelProperty(notes = "lastName", name = "lastName", required = true, value = "Komilov")
    String lastName;

    @ApiModelProperty(notes = "lastName", name = "lastName", required = true, value = "Komilov")
    String middleName;

    @Pattern(regexp = "[A-Z]{2}[0-9]{7}", message = "passport number is invalid")
    String passportNumber;

    List<ResUploadFile> photo;

    String username;

    String language;


}
