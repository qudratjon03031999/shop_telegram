package com.example.shop_bot.payload;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

/**
 * @description: TODO
 * @projectName koinot_market
 * @date: 15 February 2022 $
 * @time: 22:59 $
 * @author: Qudratjon Komilov
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "user login object")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ReqUser {

    @NotNull
    @NotBlank()
    @Pattern(regexp = "^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\\\\\\\\s\\\\\\\\./0-9]*$", message = "telephone number is invalid")
    @ApiModelProperty(notes = "phone number ", name = "phoneNumber", required = true, value = "+998917797278")
    String phoneNumber;

    @NotNull(message = "password must be not null")
    @NotBlank(message = "password is empty")
    @Length(min = 4, max = 30, message = "password is invalid length is min = 4 max = 30")
    @ApiModelProperty(notes = "password", name = "password", required = true, value = "koinot")
    String password;


    String firstName;

    String lastName;
}
