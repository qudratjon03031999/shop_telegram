package com.example.shop_bot.payload;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

/**
 * @author Komilov Qudrtajon
 * @link Telegram Link https://t.me/qudratjon03031999
 * @since 31/01/22
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class DataPageable {
    int number;
    boolean last;
    int size;
    int numberOfElements;
    int totalPages;
    Pageable pageable;
    Sort sort;
    boolean first;
    long totalElements;
    boolean empty;
    Object content;
}
